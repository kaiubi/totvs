# README

## Requirements
	The exercise was developed in python 3
	Install the required dependencies with pip install requirements.txt

## Description
	1. Parse and extract the data.
		The JSON data was parsed in a pandas DataFrame
		A few rows with outliers were removed

	2. Identify a pattern on any set of fields that can help predict how much a customer will spend.
		A Linear Regression Model was used to predict how much a customer will spend.
		There were only a few variables that could be used as explandatory variables for this model since we don't have any personal information in advance.
	
		Some helper variables were calculated to help with the training model, for instance:
			- time: a continuous numeric variable for the time of the day
			- date: a continuous numeric variable for day
			- after_4pm: a boolean variable that dictates whether the time of the purchases was befor or after 4pm.
						 (The average total value from the sales after 4pm were slightly higher than the sales that took place before 4pm)
			- weekday: identifies the day of the week from the raw "date" variable

	3. Calculate a sales forecast for the next week
		Here the ARIMA Time Series forecast algorithm was used to predict the total sales for each day of the next week
		By plotting the total sales per day a periodic (weekly) trend can be noticed.
	