import json
import numpy as np
import pandas as pd
import datetime as dt
import statsmodels.api as sm
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, r2_score


# Calculate a countinuous numeric time variable
def seconds_from_midnight(date):
    midnight = date.replace(hour=0, minute=0, second=0, microsecond=0)
    return int((date - midnight).seconds)


# Trains a LinearRegression model to predict how much a customer will spend
def getRegressionModel(df):
    # Pre-process raw variables and add calculated variables
    df = df.set_index(['sale_id'])
    df['time'] = df['date'].map(seconds_from_midnight)
    df['weekday'] = df['date'].map(dt.datetime.weekday)
    df['after_4pm'] = [0 if date.hour < 16 else 1 for date in df.date]
    df['date'] = df['date'].map(dt.datetime.toordinal)

    # Adds dummy variables
    df = df.join(pd.get_dummies(df['weekday'], prefix='week'))
    df = df.join(pd.get_dummies(df['infCpl'], prefix='table'))

    # Removes auxiliary non-numeric variables
    df.drop('weekday', axis=1, inplace=True)    
    df.drop('infCpl', axis=1, inplace=True)

    # Calculates the correlation between the explandatory variables and the "Total" response variable
    corr_matrix = df.corrwith(df.total).abs().sort_values(ascending = False)

    # Gets the most informative variables
    df = df[corr_matrix[:20].index]

    # Separates the explandatory from the response variable in the DataFrame
    X = df.loc[:, df.columns != 'total']
    y = df.loc[:, 'total'].values

    # Separates between Training and Test sets (80%-20%)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    ## Create linear regression object
    regr = linear_model.LinearRegression()

    ## Train the model using the training sets
    regr.fit(X_train, y_train)

    ## Make predictions using the testing set
    y_pred = regr.predict(X_test)

    print('\n\n2 - Identify a pattern on any set of fields that can help predict how much a customer will spend.')
    print("\tMean absolute error: %.2f" % mean_absolute_error(y_test, y_pred))
    print('\tVariance score: %.2f' % r2_score(y_test, y_pred))

    return regr


def getNextWeekForecast(df):
    # Resamples by day
    df = df[['total', 'date']]
    df = df.set_index(['date'])
    df = df['total'].resample('D').sum()
    df = df.astype('float')

    # Fill NaN values
    df[df == 0] = np.nan
    df = df.fillna(df.bfill())

    # Creates the time series forecast model
    mod = sm.tsa.statespace.SARIMAX(df, order=(1, 1, 1), seasonal_order=(0, 1, 0, 7))
    results = mod.fit(disp=0)

    # Get forecast for next week sales
    predicted = results.get_forecast(steps=7).predicted_mean[1:]

    # Prints the result
    print("\n\nCalculate a sales forecast for the next week.")
    for index, value in predicted.iteritems():
        print('\t{}: {:0.2f}'.format(index.strftime('%Y-%m-%d'), value))

    return predicted


if __name__ == '__main__':
    # Opens the sample file
    with open('sample.txt') as f:
        data = json.loads(f.read())

    # Parses the input file
    # List comprehension wouldn't be advisable with a more heterogeneous data set
    df = pd.concat(pd.DataFrame([{
        'sale_id' : sale_id,
        'date': el['ide']['dhEmi']['$date'],
        'infCpl': el['infAdic']['infCpl'],
        'total': el['complemento']['valorTotal']
    }]) for sale_id, el in enumerate(data))

    # Parses the date type
    df['date'] = pd.to_datetime(df.date)

    # Clears outliers\n\n
    df = df[df.total < 300]

    print ("\n\n1 - Parse and extract the data.")

    # This model could be used to predict how much new customers will spend
    model = getRegressionModel(df)

    # This array contains the forecast for next week sales
    predicted = getNextWeekForecast(df)
